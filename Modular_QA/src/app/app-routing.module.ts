import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './layout/default/default.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { PostsComponent } from './modules/posts/posts.component';
import { GraphsComponent } from './modules/graphs/graphs.component';
import { BarChartComponent } from './modules/graphs/bar-chart/bar-chart.component';
import { PieChartComponent } from './modules/graphs/pie-chart/pie-chart.component';



const routes: Routes = [
{
    path: '',
    component: DefaultComponent,
    children: [{
      path: '',
      component: DashboardComponent
    },
    {
      path: 'posts',
      component: PostsComponent
    },
    {
      path: 'graphs',
      component: GraphsComponent,
      children: [{
        path: 'bar-chart',
        loadChildren: () => BarChartComponent
      },
      {
        path: 'pie-chart',
        loadChildren: () => PieChartComponent

      }]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const moduleRoutes = RouterModule.forChild(routes);
