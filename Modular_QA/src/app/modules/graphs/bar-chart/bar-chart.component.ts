import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public barChartLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType = 'bar';
  public barChartLegend = 'true';
  public barChartData = [{data: [65, 68, 78, 34, 32, 53, 45, 66, 55], label: ' Series A'},
                         {data: [69, 43, 22, 31, 33, 59, 75, 60, 58], label: ' Series B'}];

  constructor() { }

  ngOnInit() {
  }

}
