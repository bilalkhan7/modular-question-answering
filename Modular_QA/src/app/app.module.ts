import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DefaultModule } from './layout/default/default.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BarChartComponent } from './modules/graphs/bar-chart/bar-chart.component';
import { PieChartComponent } from './modules/graphs/pie-chart/pie-chart.component';



@NgModule({
  declarations: [
    AppComponent,
    BarChartComponent,
    PieChartComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DefaultModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
