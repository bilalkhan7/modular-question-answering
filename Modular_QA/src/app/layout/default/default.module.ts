import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DefaultComponent } from './default.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DashboardComponent } from 'src/app/modules/dashboard/dashboard.component';
import { PostsComponent } from 'src/app/modules/posts/posts.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardService } from 'src/app/modules/dashboard.service';
import { MatSidenavModule , MatDividerModule, MatTableModule, MatPaginatorModule, MatFormFieldModule , MatInputModule,
         MatAutocompleteModule } from '@angular/material';
import { GraphsComponent } from 'src/app/modules/graphs/graphs.component';




@NgModule({
  declarations: [
  DefaultComponent,
  DashboardComponent,
  PostsComponent,
  GraphsComponent

],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatSidenavModule,
    MatDividerModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    FormsModule

  ],
  providers: [
    DashboardService
  ]
})
export class DefaultModule { }
